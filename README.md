# 20 JAVASCRIPT DESIGN PATTERNS
url: https://www.udemy.com/javascript-design-patterns-20-patterns-for-expert-code/learn/v4/overview


## {{{  My First Design Patterns - _Controlling the Global Access_ }}}
In software engineering, a software design pattern is a general, reusable solution to a commonly occurring problem within a given context in software design. It is not a finished design that can be transformed directly into source or machine code. It is a description or template for how to solve a problem that can be used in many different situations. Design patterns are formalized best practices that the programmer can use to solve common problems when designing an application or system.
###### External resources:
- https://en.wikipedia.org/wiki/Software_design_pattern

### 01- Object Literal Pattern
A JavaScript object literal is a comma-separated list of name-value pairs wrapped in curly braces. Object literals encapsulate data, enclosing it in a tidy package. This minimizes the use of global variables which can cause problems when combining code. Object literal property values can be of any data type, including array literals, functions, and nested object literals.
###### External resources:
- https://dmitripavlutin.com/why-object-literals-in-javascript-are-cool/
- https://www.sitepoint.com/es6-enhanced-object-literals/


### 02- Namespace Design Pattern
Namespaces can be considered a logical grouping of units of code under a unique identifier. The identifier can be referenced in many namespaces and each identifier can itself contain a hierarchy of its own nested (or sub) namespaces.
In application development, we employ namespaces for a number of important reasons. In JavaScript, they help us avoid collisions with other objects or variables in the global namespace. They're also extremely useful for helping organize blocks of functionality in a code-base so that it can be more easily referenced and used.
Namespacing any serious script or application is critical as it's important to safeguard our code from breaking in the event of another script on the page using the same variable or method names we are. 
###### External resources:
- https://www.safaribooksonline.com/library/view/learning-javascript-design/9781449334840/ch13s15.html
- https://addyosmani.com/blog/essential-js-namespacing/


### 03- Module Design Pattern
The Module pattern was originally defined as a way to provide both private and public encapsulation for classes in conventional software engineering.
In JavaScript, the Module pattern is used to further emulate the concept of classes in such a way that we're able to include both public/private methods and variables inside a single object, thus shielding particular parts from the global scope. What this results in is a reduction in the likelihood of our function names conflicting with other functions defined in additional scripts on the page.
###### External resources:
- http://jargon.js.org/_glossary/MODULE_PATTERN.md
- https://medium.com/@hidace/cleaning-up-javascript-with-modular-design-patterns-487266a386b3
- https://toddmotto.com/mastering-the-module-pattern/


### 04- Revealing Module Design Pattern
This pattern is the same concept as the module pattern in that it focuses on public & private methods. The only difference is that the revealing module pattern was engineered as a way to ensure that all methods and variables are kept private until they are explicitly exposed; usually through an object literal returned by the closure from which it’s defined.
###### External resources:
- http://jargon.js.org/_glossary/REVEALING_MODULE_PATTERN.md
- https://medium.com/@joshuacoquelle/revelling-in-the-revealing-module-pattern-the-revealing-module-pattern-is-one-of-my-favourite-6fe8ce60f715
- https://toddmotto.com/mastering-the-module-pattern/#revealing-module-pattern
- https://weblogs.asp.net/dwahlin/techniques-strategies-and-patterns-for-structuring-javascript-code-revealing-module-pattern

##### _Difference between Module Design Pattern and Revealing Module Design Pattern_
- https://stackoverflow.com/questions/22906662/javascript-design-pattern-difference-between-module-pattern-and-revealing-modul#




## {{{ Creational Design Patterns }}}
In software engineering, creational design patterns are design patterns that deal with object creation mechanisms, trying to create objects in a manner suitable to the situation. The basic form of object creation could result in design problems or in added complexity to the design. Creational design patterns solve this problem by somehow controlling this object creation.
The creational patterns aim to separate a system from how its objects are created, composed, and represented. They increase the system's flexibility in terms of the what, who, how, and when of object creation.

### 05- Singleton Design Pattern
###### External resources:
- link


### 06- Factory Design Pattern
###### External resources:
- link


### 07- Abstract Factory Design Pattern
###### External resources:
- link


### 08- Builder Design Pattern
###### External resources:
- link


### 09- Prototype Design Pattern
###### External resources:
- link




## {{{ Structural Design Patterns }}}
In software engineering, structural design patterns are design patterns that ease the design by identifying a simple way to realize relationships between entities. 
Structural design patterns are responsible for building simple and efficient class hierarchies and relations between different classes.
Focus on the architecture of the whole application

### 10- Abstracting Singleton Design Pattern
###### External resources:
- link


### 11- Adapter Design Pattern
###### External resources:
- link


### 12- Composite Design Pattern
###### External resources:
- link


### 13- Decorator Design Pattern
###### External resources:
- link


### 14- The Fly Weight Design Pattern
###### External resources:
- link


### 15- The Façade Design Pattern 
###### External resources:
- link


### 16- The Bridge Design Pattern
###### External resources:
- link


### 17- The Proxy Design Pattern
###### External resources:
- link

## {{{ Behavioral Design Patterns }}}
In software engineering, behavioral design patterns are design patterns that identify common communication patterns between objects and realize these patterns. By doing so, these patterns increase flexibility in carrying out this communication.

### 18- The Chain of Responsibility Design Pattern
###### External resources:
- link


### 19- The Observer Design Pattern
###### External resources:
- link


### 20- The State Design Pattern
###### External resources:
- link



### External resources:
- https://addyosmani.com/resources/essentialjsdesignpatterns/book/
- https://refactoring.guru/design-patterns/
- https://sourcemaking.com/design_patterns/
- https://02geek.com/catalog